package com.lev.book_storage;


import static org.assertj.core.api.Assertions.assertThat;

import com.lev.book_storage.dao.AppUserRepository;
import com.lev.book_storage.domain.AppUser;
import com.lev.book_storage.domain.Role;
import com.lev.book_storage.domain.Status;
import com.lev.book_storage.services.appUsers.AppUserRegistrationService;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
public class AuthUtilControllerTests {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private AppUserRegistrationService appUserRegistrationService;


    @Test
    public void NotNullTest(){
        assertThat(appUserRepository).isNotNull();
        assertThat(appUserRegistrationService).isNotNull();
    }

    @Test
    public void UserIsRegisteredSuccessfully(){
        AppUser appUser = new AppUser(
                "mail@mail.ru",
                "password",
                Role.ADMIN,
                Status.ACTIVE
        );
        appUserRegistrationService.register(appUser);
        Optional<AppUser> SavedAppUser = appUserRepository.findByEmail(appUser.getEmail());
        assertThat(SavedAppUser).isNotNull();
    }

}

const form = document.getElementById("form");

const email = document.getElementById("email");
const psw = document.getElementById("psw");
const pswRepeat = document.getElementById("psw-repeat");

function userRegister(userData) {
  fetch("http://localhost:8080/api/v1/registration", {
    method: "POST",
    body: JSON.stringify(userData),
  });
}

form.addEventListener("submit", (event) => {
  event.preventDefault();

  if (pswRepeat.value !== psw.value) {
    return false;
  }

  const userObj = {
    email: email.value,
    pasword: email.value,
  };

  userRegister(userObj);
});

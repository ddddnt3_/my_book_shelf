package com.lev.book_storage.domain;

public enum BookStatus {
    COMPLETED,
    ABANDONED,
    IN_PROGRESS,
    TO_BE_READ;
}

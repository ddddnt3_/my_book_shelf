package com.lev.book_storage.domain;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name ="email", nullable = false)
    private String email;
    @Column(name ="password", nullable = false)
    private String password;
    @Column(name ="role", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Column(name ="status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @OneToOne(mappedBy = "appUser")
    private Book book;

    public AppUser(String email,
                   String password,
                   Role role,
                   Status status) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.status = status;
    }
}


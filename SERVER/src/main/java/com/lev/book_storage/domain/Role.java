package com.lev.book_storage.domain;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.Set;
import java.util.stream.Collectors;

public enum Role {
    USER(Set.of(Permission.APP_USERS_READ)),
    ADMIN(Set.of(Permission.APP_USERS_WRITE, Permission.APP_USERS_READ));


    Role(Set<Permission> permission) {
        this.permission = permission;
    }

    public Set<Permission> getPermissions() {
        return permission;
    }

    private final Set<Permission> permission;


    public Set<SimpleGrantedAuthority> getAuthorities(){
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }

}

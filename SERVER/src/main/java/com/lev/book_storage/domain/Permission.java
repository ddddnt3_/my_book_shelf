package com.lev.book_storage.domain;

public enum Permission {

    APP_USERS_READ("app_users:read"),
    APP_USERS_WRITE("app_users:write");

    private final String permission;


    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}

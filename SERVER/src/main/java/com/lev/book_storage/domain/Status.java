package com.lev.book_storage.domain;

public enum Status {
    ACTIVE,
    BANNED;
}

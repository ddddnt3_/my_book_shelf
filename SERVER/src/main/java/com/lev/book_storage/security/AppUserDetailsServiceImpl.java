package com.lev.book_storage.security;

import com.lev.book_storage.dao.AppUserRepository;
import com.lev.book_storage.domain.AppUser;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("userDetailsServiceImpl")
public class AppUserDetailsServiceImpl implements UserDetailsService {


    private final AppUserRepository userRepository;

    @Autowired
    public AppUserDetailsServiceImpl(AppUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User does not exist"));
        return SecurityAppUser.fromUser(appUser);
    }
}

package com.lev.book_storage.utils.http_responder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HttpStatusResponse {

    private short httpStatus;
    private String msg;

}

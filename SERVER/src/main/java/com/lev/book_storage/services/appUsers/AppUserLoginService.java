package com.lev.book_storage.services.appUsers;

import com.lev.book_storage.dao.AppUserRepository;
import com.lev.book_storage.domain.AppUser;
import com.lev.book_storage.dto.AuthenticationRequestDTO;
import com.lev.book_storage.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Component
public class AppUserLoginService {

    private JwtTokenProvider jwtTokenProvider;
    private AppUserRepository appUserRepository;
    private AuthenticationManager authenticationManager;
    @Autowired
    public AppUserLoginService(JwtTokenProvider jwtTokenProvider, AppUserRepository appUserRepository, AuthenticationManager authenticationManager) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.appUserRepository = appUserRepository;
        this.authenticationManager = authenticationManager;
    }

    public ResponseEntity<?> authenticate(AuthenticationRequestDTO request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            AppUser user = appUserRepository.findByEmail(request.getEmail()).orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
            String token = jwtTokenProvider.createToken(request.getEmail(), user.getRole().name());
            Map<Object, Object> response = new HashMap<>();
            response.put("email", request.getEmail());
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Invalid email/password combination", HttpStatus.FORBIDDEN);
        }
    }

}

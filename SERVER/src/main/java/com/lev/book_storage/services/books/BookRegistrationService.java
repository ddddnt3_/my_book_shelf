package com.lev.book_storage.services.books;

import com.lev.book_storage.dao.BookRepository;
import com.lev.book_storage.domain.Book;
import com.lev.book_storage.domain.BookStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookRegistrationService {

    private final BookRepository bookRepository;

    public String prepareABook(Book request) {
        boolean bookNameExists = bookRepository.findBookByName(request.getName()).isPresent();
        if (bookNameExists) throw new IllegalStateException("Book name already taken.");
        bookRepository.save(request);
        return "success";

//TODO make json response method not related to any particular class and make implementation flexible for any class

    }

    public List<Book> getAllBooksByAuthor(String author){
        return List.of(
                bookRepository.findBookByAuthor(author)
        );

    }

    public List<Book> getBookByStatus(String status){
        BookStatus bookStatus = switch (status) {
            case "COMPLETED" -> BookStatus.COMPLETED;
            case "ABANDONED" -> BookStatus.ABANDONED;
            case "TO_BE_READ" -> BookStatus.TO_BE_READ;
            case "IN_PROGRESS" -> BookStatus.IN_PROGRESS;
            default -> throw new IllegalArgumentException("Bad requested status");
        };

        return List.of(
                bookRepository.findByStatus(bookStatus)
        );

    }

    public List<Book> getAllBooksByStatus(String status){
        BookStatus bookStatus = switch (status) {
            case "COMPLETED" -> BookStatus.COMPLETED;
            case "ABANDONED" -> BookStatus.ABANDONED;
            case "TO_BE_READ" -> BookStatus.TO_BE_READ;
            case "IN_PROGRESS" -> BookStatus.IN_PROGRESS;
            default -> throw new IllegalArgumentException("Bad requested status");
        };

        return bookRepository.findAllByStatus(bookStatus);

    }
}

package com.lev.book_storage.services.appUsers;
import com.lev.book_storage.dao.AppUserRepository;
import com.lev.book_storage.domain.AppUser;
import com.lev.book_storage.utils.EmailValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Getter
public class AppUserRegistrationService {

    private final EmailValidator emailValidator;
    private final AppUserRepository appUserRepository;
    private final DelegatingPasswordEncoder delegatingPasswordEncoder;

    public ResponseEntity<?> register(AppUser request) {
        boolean isValidEmail = emailValidator.test(request.getEmail());
        if (!isValidEmail) throw new IllegalStateException("Email is not valid.");
        return signUpAppUser(
                new AppUser(request.getEmail(),
                        request.getPassword(),
                        request.getRole(),
                        request.getStatus()
                )

        );
    }

    public ResponseEntity<?> signUpAppUser(AppUser appUser){
        boolean userExists = appUserRepository.findByEmail(appUser.getEmail()).isPresent();
        if (userExists) throw new IllegalStateException("Email already taken.");
        String encodedPassword = delegatingPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        appUserRepository.save(appUser);
        Map<Object, Object> response = new HashMap<>();
        response.put("email added", appUser.getEmail());
        response.put("http status",HttpStatus.CREATED);
        return ResponseEntity.ok(response);
    }

}


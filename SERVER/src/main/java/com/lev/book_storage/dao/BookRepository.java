package com.lev.book_storage.dao;

import com.lev.book_storage.domain.Book;
import com.lev.book_storage.domain.BookStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    Optional <Book> findBookByName(String book_name);
    Book findBookByAuthor(String author);

    Book findByStatus(BookStatus status); //return a single item based in status

    //@Query("SELECT u FROM Book u WHERE u.status = ?1")
    List<Book> findAllByStatus(BookStatus status);

}

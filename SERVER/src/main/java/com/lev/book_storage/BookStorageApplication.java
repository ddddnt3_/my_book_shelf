package com.lev.book_storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class BookStorageApplication {

    private static final Logger logger = Logger.getLogger(BookStorageApplication.class.getName());

    public static void main(String[] args) {

        SpringApplication.run(BookStorageApplication.class, args);
        logger.info("Application running");

    }

}

package com.lev.book_storage.controllers.rest;

import com.lev.book_storage.domain.*;
import com.lev.book_storage.services.books.BookRegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/books")
@RequiredArgsConstructor
public class BookRestController {

    private final BookRegistrationService bookRegistrationService;

    private List<Book> BOOK = Stream.of(
            new Book("name","author",BookStatus.ABANDONED)
    ).collect(Collectors.toList());


    @GetMapping("/")
    public List<Book> getListOfBooks(){
        return BOOK;
    }
    @PostMapping("/new_book")
    public String new_book(@RequestBody Book book){
        return bookRegistrationService.prepareABook(book);
    }

    //TODO rewor the method to work as his brother methods from below
    @GetMapping("/get_books_by_author/{author}")
    public List<Book> getAllBooksByAuthor(@PathVariable String author){
        return bookRegistrationService.getAllBooksByAuthor(author);
    }

    @GetMapping("/get_a_book_by_status")
    public List<Book> getBookByStatus(@RequestParam String status){
        return bookRegistrationService.getBookByStatus(status);
    }
    @GetMapping("/get_books_by_status")
    public List<Book> getAllBooksByStatus(@RequestParam String status){
        return bookRegistrationService.getAllBooksByStatus(status);
    }
}

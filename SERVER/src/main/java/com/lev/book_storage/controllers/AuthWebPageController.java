package com.lev.book_storage.controllers;

//TODO class to be deleted
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/auth")
public class AuthWebPageController {

    @GetMapping("/login")
    public String getLoginPage(){
        return "login";
    }

    @GetMapping("/success")
    public String getSuccessPage(){
        return "success";
    }

    @GetMapping("/registration")
    public String getRegisterPage() {
        return "registration";
    }

}


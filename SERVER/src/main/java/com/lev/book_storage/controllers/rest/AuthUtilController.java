package com.lev.book_storage.controllers.rest;


import com.lev.book_storage.domain.AppUser;
import com.lev.book_storage.dto.AuthenticationRequestDTO;
import com.lev.book_storage.services.appUsers.AppUserLoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lev.book_storage.services.appUsers.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthUtilController {

    private final AppUserRegistrationService appUserRegistrationService;
    private final AppUserLoginService appUserLoginService;

    public AuthUtilController(AppUserRegistrationService appUserRegistrationService,
                              AppUserLoginService appUserLoginService) {
        this.appUserRegistrationService = appUserRegistrationService;
        this.appUserLoginService = appUserLoginService;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> register(@RequestBody AppUser request) {
        return appUserRegistrationService.register(request);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authentication(@RequestBody AuthenticationRequestDTO request) {
        return appUserLoginService.authenticate(request);
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        var securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}

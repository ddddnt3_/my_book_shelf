# Presenting BookShelf

The BookShelf is created to help its users to manage their reading. 
The service provides an easy-to-understand GUI and efficient usability via drag and drop system. 
The user is able to set what books are being read currently and which are yet to be read. 
You can also track what books you already went through and which ones are abandoned for any reason.
While this project is meant to be a "pet project" with sole purpose of showcasing my Java developer skills, I plan on using it myself and improving it for as long as I can.

# Features

Here is what you can do:
+ Drag and drop your books between columns depending on its status: READING, COMPLETED, ABANDONED, TO_BE_READ
+ Sort the columns by many parameters
+ Add comments to books to remember your thoughts
+ Amuse your ego on how many books you have read
# Technology used

### Backend tools:
+ Java 11+
+ Spring Framework
  + Spring Boot 
  + Spring Data JPA (Hibernate) 
  + Spring Security (JWT)
+ Maven
+ PostgresQL
+ Junit


### Frontend tools:
+ TypeScript
+ React
  + ReactRouterDom
+ Redux
+ ReactBeautifulDnd 
+ Tailwind
+ Vite

# The team

Responsible for **backend** development: [@narcissus_the_flower](https://github.com/tseby)

Responsible for **frontend** development: [@Secret_Targryen](https://github.com/StTargaryen)

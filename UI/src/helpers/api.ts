export const baseUrl = "http://localhost:8080/api/v1";

enum RequestMethods {
  get = "GET",
  post = "POST",
  put = "PUT",
  delete = "DELETE",
}

class Api {
  public async get<T>(url: string): Promise<T> {
    return fetch(url, {
      headers: {
        "Content-Type": "application/json",
      },
      method: RequestMethods.get,
    }).then((response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json() as Promise<T>;
    });
  }
}

export const api = new Api();

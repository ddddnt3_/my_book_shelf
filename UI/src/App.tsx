import React, { useState } from 'react'
import Bookshelf from './components/Bookshelf';

import './styles/App.css'

const App = () => {
  // На данный момент добавлена обычная верстка. Позднее логика будет распределена на отдельные функциональные компоненты.
  return (
    <div className="App">
      <header className='header'>
        <div className='content-wrapper'>
          <nav className="header-nav">
            <ul className='header-nav__list'>
              <li className="header-nav__list-item"><a href="#">Bookshelf</a></li>
              <li className="header-nav__list-item"><a href="#">Search</a></li>
              <li className="header-nav__list-item"><a href="#">Some nav-item</a></li>
            </ul>
          </nav>
          <button className="header-tools">
            <div className="header-circle"></div>
            <div className="dropdown"></div>
          </button>
        </div>
      </header>
      <main className='main'>
        <div className="content-wrapper">
          <div className='bookshelf'>
            <aside className="bookshelf__sidebar">
              <div className="bookshelf__sidebar-header">Filters</div>
              <div className="bookshelf__sidebar-content-wrapper">
                <div className="bookshelf__filter">
                  <div className='bookshelf__filter-search'>
                    <input type="text" />
                    <svg className="" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                  </div>
                  <ul className='bookshelf__filter-list'>
                    <li className='bookshelf__filter-list-item'>
                      <span><svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg></span>
                      <span>My Vault</span>
                    </li>
                  </ul>
                </div>
              </div>
            </aside>
            <div className='bookshelf-content'>
              <div className="bookshelf-title">Vault Items</div>
              <Bookshelf />
            </div>
          </div>
        </div>
      </main >
      <footer className='footer'>
        <div className='content-wrapper' >
          <div className="footer-content">Version 0.1</div>
        </div>
      </footer>
    </div >
  )
}

export default App

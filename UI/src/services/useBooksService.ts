import { useEffect, useState } from "react";
import { Service } from "../types/Service";
import { Book } from "../types/Book";
import { api, baseUrl } from "../helpers/api";

export interface Books {
  results: Book[];
}

const useBooksService = (status: string) => {
  const [result, setResult] = useState<Service<Books>>({
    status: "loading",
  });

  useEffect(() => {
    api
      .get<Book>(`${baseUrl}/books/get_books_by_status?status=${status}`)
      .then((res) => setResult({ status: "loaded", payload: { results: res } }))
      .catch((error) => setResult({ status: "error", error }));
  }, []);

  return result;
};

export default useBooksService;

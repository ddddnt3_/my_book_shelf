enum BookStatus {
  COMPLETED,
  ABANDONED,
  IN_PROGRESS,
  TO_BE_READ,
}

export interface Book {
  id: number;
  name: string;
  author: string;
  status: BookStatus;
}

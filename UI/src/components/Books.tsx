import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { Book } from '../types/Book';

import owrellImage from '../assets/owrell.jpeg';

interface BookProps {
  isLoading: boolean,
  isError: boolean,
  items: Book[]
}

const Books: React.FC<BookProps> = ({ items, isLoading, isError }: BookProps) => {
  return (
    <>
      <div style={{ color: '#ffffff' }}>
        {isLoading}
      </div>
      <div className="card">
        {isLoading && (
          <div className="loader-container">
            Загрузка
          </div>
        )}
        {
          !isLoading && !isError &&
          items.map((book, index) => (
            <Draggable
              key={book.id}
              draggableId={JSON.stringify(book.id)}
              index={index}
            >
              {(provided, snapshot) => {
                return (
                  <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    style={{
                      userSelect: "none",
                      padding: 16,
                      margin: "0 0 8px 0",
                      minHeight: "50px",
                      backgroundColor: snapshot.isDragging
                        ? "#263B4A"
                        : "",
                      // color: "white",
                      ...provided.draggableProps.style
                    }}
                    className="bookshelf__list-item"
                  >
                    <svg className="bookshelf__list-item-check" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 8h16M4 16h16"></path></svg>
                    <span className='bookshelf__list-item-image' style={
                      {
                        backgroundImage: `url(${owrellImage})`
                      }
                    } />
                    <div className="bookshelf__list-item-content">
                      <span className='bookshelf__list-item-title'>{book.name}</span>
                      <span className='bookshelf__list-item-author'>{book.author}</span>
                    </div>
                  </div>
                );
              }}
            </Draggable>
          ))}
      </div>
      {
        isError && !isLoading && (
          <div>Error, the server send memes.</div>
        )
      }
    </>
  );
};

export default Books;
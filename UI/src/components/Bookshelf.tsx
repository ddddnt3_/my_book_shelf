import React, { useState, useEffect, useMemo } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { api, baseUrl } from "../helpers/api";
import Books from "./Books";
import { Book } from '../types/Book';


enum ColumnStatus {
  completed = 'COMPLETED',
  abandoned = 'ABANDONED',
  inProgress = 'IN_PROGRESS',
  toBeRead = 'TO_BE_READ',
}


const booksColumns = {
  ['1']: {
    name: "Completed Books",
    items: [],
    status: ColumnStatus.completed,
    isLoading: false,
    isError: false
  },
  ['2']: {
    name: "Abandoned books",
    items: [],
    status: ColumnStatus.abandoned,
    isLoading: false,
    isError: false
  },
  ['3']: {
    name: "Reading currently",
    items: [],
    status: ColumnStatus.inProgress,
    isLoading: false,
    isError: false
  },
  ['4']: {
    name: "Feature books",
    items: [],
    status: ColumnStatus.toBeRead,
    isLoading: false,
    isError: false
  }
};

const onDragEnd = (result, columns, setColumns) => {
  if (!result.destination) return;
  const { source, destination } = result;

  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);

    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems
      },
      [destination.droppableId]: {
        ...destColumn,
        items: destItems
      }
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        items: copiedItems
      }
    });
  }
};

const Bookshelf = () => {
  const [columns, setColumns] = useState(booksColumns);

  useEffect(() => {
    const newColumns = {};
    Object.entries(columns).forEach(([colId, col]) => {
      setColumns({
        ...columns, ...newColumns,
        [colId]: { ...col, isLoading: true }
      })
      
      api
        .get<Book>(`${baseUrl}/books/get_books_by_status?status=${col.status}`)
        .then((res) => {
          newColumns[colId] = {
            ...col,
            isLoading: false,
            items: res,
          }
          setColumns({ ...columns, ...newColumns })
        })
        .catch((error) => console.error(error));

    });
  }, []);

  return (
    <div className="bookshelf-books">
      <DragDropContext
        onDragEnd={result => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([columnId, column], index) => {
          return (
            <div
              className="bookshelf__col"
              key={columnId}
            >
              <h2 className="bookshelf__col-title">{column.name}</h2>
              <div className="bookshelf__list">
                <Droppable droppableId={columnId} key={columnId}>
                  {(provided, snapshot) => {
                    return (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                          background: snapshot.isDraggingOver
                            ? "lightblue"
                            : "",
                          padding: 4,
                          width: '100%',
                          height: '100%',
                          minHeight: 500,
                          borderRadius: '4px'
                        }}
                      >
                        <Books items={column.items} isError={column.isError} isLoading={column.isLoading} />
                        {provided.placeholder}
                      </div>
                    );
                  }}
                </Droppable>
              </div>
            </div>
          );
        })}
      </DragDropContext>
    </div>
  );
}

export default Bookshelf;
